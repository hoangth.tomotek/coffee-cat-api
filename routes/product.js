const express = require("express");
const connection = require("../connection.js");
const router = express.Router();
const multer = require("multer");
const path = require("path");
const auth = require("../services/authentication.js");
const checkRole = require("../services/checkRole.js");
const Helper = require("../helper.js").Helper;

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads/");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + path.extname(file.originalname));
  },
});

const upload = multer({ storage: storage });
const UPLOADS_PATH = `http://localhost:${process.env.PORT}/uploads/`;

router.post(
  "/add",
  auth.authenticateToken,
  checkRole.checkRole,
  upload.single("image"),
  (req, res, next) => {
    let name = req.body.name;
    const categoryId = req.body.categoryId;
    const description = req.body.description;
    const price = req.body.price;
    const image = req.file ? req.file.filename : null;

    if (!name || !categoryId || !description || !price || !image) {
      return res
        .status(400)
        .json({ message: "All fields are required, including image" });
    }

    name = name.toUpperCase();

    let checkNameQuery = "SELECT * FROM product WHERE name = ?";
    connection.query(checkNameQuery, [name], (err, results) => {
      if (err) {
        return Helper.dbErrorReturn(err, res);
      }
      if (results.length > 0) {
        return res.status(400).json({ message: "Product name already exists" });
      }

      let query =
        "INSERT INTO product (name, categoryId, description, price, image) VALUES (?,?,?,?,?)";
      connection.query(
        query,
        [name, categoryId, description, price, image],
        (err, results) => {
          if (!err) {
            return res
              .status(200)
              .json({ message: "Product added successfully" });
          } else {
            return Helper.dbErrorReturn(err, res);
          }
        }
      );
    });
  }
);

module.exports = router;

router.get("/get", (req, res, next) => {
  let query =
    "SELECT p.id, p.name, p.description, p.price, p.image, p.status, c.id AS categoryId, c.name AS categoryName" +
    " FROM product p LEFT JOIN category c ON p.categoryId = c.id WHERE p.status=1";
  connection.query(query, (err, results) => {
    if (!err) {
      results = results.map((product) => {
        product.image = UPLOADS_PATH + product.image;
        return product;
      });
      return res.status(200).json(results);
    } else {
      return Helper.dbErrorReturn(err, res);
    }
  });
});

router.get("/getByCategory/:id", (req, res, next) => {
  const id = req.params.id;
  let query =
    "SELECT id, name, price, image, description FROM product p WHERE categoryId=? AND status=1";
  connection.query(query, [id], (err, results) => {
    if (!err) {
      results = results.map((product) => {
        product.image = UPLOADS_PATH + product.image;
        return product;
      });
      return res.status(200).json(results);
    } else {
      return Helper.dbErrorReturn(err, res);
    }
  });
});

router.get("/getById/:id", (req, res, next) => {
  const id = req.params.id;
  let query =
    "SELECT id, name, description, price, image FROM product p WHERE id=? AND status=1";
  connection.query(query, [id], (err, results) => {
    if (!err) {
      results = results.map((product) => {
        product.image = UPLOADS_PATH + product.image;
        return product;
      });
      return res.status(200).json(results[0]);
    } else {
      return Helper.dbErrorReturn(err, res);
    }
  });
});

router.patch(
  "/update",
  auth.authenticateToken,
  checkRole.checkRole,
  upload.single("image"),
  (req, res, next) => {
    const id = req.body.id;
    const name = req.body.name;
    const categoryId = req.body.categoryId;
    const description = req.body.description;
    const price = req.body.price;
    const image = req.file ? req.file.filename : null;

    if (!id) {
      return res.status(400).json({ message: "Product ID is required" });
    }

    let checkProduct = "SELECT * FROM product WHERE id = ?";
    connection.query(checkProduct, [id], (err, result) => {
      if (err) {
        return Helper.dbErrorReturn(err, res);
      }

      if (result.length > 0) {
        if (name && name != result[0].name) {
          let checkName = "SELECT * FROM product WHERE name = ? AND id != ?";
          connection.query(checkName, [name, id], (err, results) => {
            if (err) {
              return Helper.dbErrorReturn(err, res);
            }
            if (results.length > 0) {
              return res
                .status(400)
                .json({ message: "Product name already exists" });
            } else {
              updateProduct(
                id,
                name,
                categoryId,
                description,
                price,
                image,
                res
              );
            }
          });
        } else {
          updateProduct(id, name, categoryId, description, price, image, res);
        }
      } else {
        return res.status(404).json({ message: "Product ID does not exist" });
      }
    });
  }
);

function updateProduct(id, name, categoryId, description, price, image, res) {
  let query = "UPDATE product SET status = 1";
  let queryParams = [];

  if (name) {
    query += " , name = ?";
    queryParams.push(name);
  }
  if (categoryId) {
    query += " , categoryId = ?";
    queryParams.push(categoryId);
  }
  if (description) {
    query += " , description = ?";
    queryParams.push(description);
  }
  if (price) {
    query += " , price = ?";
    queryParams.push(price);
  }
  if (image) {
    query += " , image = ?";
    queryParams.push(image);
  }

  query += " WHERE id = ?";
  queryParams.push(id);

  connection.query(query, queryParams, (err, results) => {
    if (err) {
      return Helper.dbErrorReturn(err, res);
    }
    return res.status(200).json({ message: "Product updated successfully" });
  });
}

router.delete(
  "/delete/:id",
  auth.authenticateToken,
  checkRole.checkRole,
  (req, res, next) => {
    const id = req.params.id;
    let query = "DELETE FROM product WHERE id=?";
    connection.query(query, [id], (err, results) => {
      if (!err) {
        if (results.affectedRows == 0) {
          return res.status(404).json({ message: "Product ID does not exist" });
        }
        return res
          .status(200)
          .json({ message: "Product deleted successfully" });
      } else {
        return Helper.dbErrorReturn(err, res);
      }
    });
  }
);

router.patch(
  "/updateStatus",
  auth.authenticateToken,
  checkRole.checkRole,
  (req, res, next) => {
    const product = req.body;
    let query = "UPDATE product SET status=? WHERE id=?";
    connection.query(query, [product.status, product.id], (err, results) => {
      if (!err) {
        if (results.affectedRows == 0) {
          return res.status(404).json({ message: "Product ID does not exist" });
        }
        return res
          .status(200)
          .json({ message: "Product status updated successfully" });
      } else {
        return Helper.dbErrorReturn(err, res);
      }
    });
  }
);

module.exports = router;
