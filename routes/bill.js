const express = require("express");
const connection = require("../connection.js");
const router = express.Router();
let ejs = require("ejs");
let pdf = require("html-pdf");
let path = require("path");
let fs = require("fs");
let uuid = require("uuid");
let auth = require("../services/authentication.js");
let checkRole = require("../services/checkRole.js");
const Helper = require("../helper.js").Helper;

router.post(
  "/generateReport",
  auth.authenticateToken,
  checkRole.checkRole,
  (req, res, next) => {
    const generatedUuid = uuid.v1();
    const orderDetails = req.body;
    let productDetailsReport = JSON.parse(orderDetails.productDetails);

    let query =
      "INSERT INTO bill(name,uuid,email,contactNumber,paymentMethod,total,productDetails,createdBy)" +
      " VALUES(?,?,?,?,?,?,?,?)";
    connection.query(
      query,
      [
        orderDetails.name,
        generatedUuid,
        orderDetails.email,
        orderDetails.contactNumber,
        orderDetails.paymentMethod,
        orderDetails.totalAmount,
        orderDetails.productDetails,
        res.locals.email,
      ],
      (err, results) => {
        if (!err) {
          ejs.renderFile(
            path.join(__dirname, "", "report.ejs"),
            {
              productDetails: productDetailsReport,
              name: orderDetails.name,
              email: orderDetails.email,
              contactNumber: orderDetails.contactNumber,
              paymentMethod: orderDetails.paymentMethod,
              totalAmount: orderDetails.totalAmount,
            },
            (err, results) => {
              if (err) {
                return Helper.dbErrorReturn(err, res);
              } else {
                pdf
                  .create(results)
                  .toFile(
                    "./generated_pdf/" + generatedUuid + ".pdf",
                    function (err, data) {
                      if (err) {
                        return Helper.dbErrorReturn(err, res);
                      } else {
                        return res.status(200).json({ uuid: generatedUuid });
                      }
                    }
                  );
              }
            }
          );
        } else {
          return Helper.dbErrorReturn(err, res);
        }
      }
    );
  }
);

router.post(
  "/getPdf",
  auth.authenticateToken,
  checkRole.checkRole,
  function (req, res, next) {
    const orderDetails = req.body;
    const pdfPath = "./generated_pdf/" + orderDetails.uuid + ".pdf";
    if (fs.existsSync(pdfPath)) {
      res.contentType("application/pdf");
      fs.createReadStream(pdfPath).pipe(res);
    } else {
      let productDetailsReport = JSON.parse(orderDetails.productDetails);
      ejs.renderFile(
        path.join(__dirname, "", "report.ejs"),
        {
          productDetails: productDetailsReport,
          name: orderDetails.name,
          email: orderDetails.email,
          contactNumber: orderDetails.contactNumber,
          paymentMethod: orderDetails.paymentMethod,
          totalAmount: orderDetails.totalAmount,
        },
        (err, results) => {
          if (err) {
            return Helper.dbErrorReturn(err, res);
          } else {
            pdf
              .create(results)
              .toFile(
                "./generated_pdf/" + "bill-" + orderDetails.uuid + ".pdf",
                function (err, data) {
                  if (err) {
                    return Helper.dbErrorReturn(err, res);
                  } else {
                    res.contentType("application/pdf");
                    fs.createReadStream(pdfPath).pipe(res);
                  }
                }
              );
          }
        }
      );
    }
  }
);

router.get("/getBills", auth.authenticateToken, (req, res, next) => {
  let query = "SELECT * FROM bill ORDER BY id DESC";
  connection.query(query, (err, results) => {
    if (!err) {
      return res.status(200).json(results);
    } else {
      return Helper.dbErrorReturn(err, res);
    }
  });
});

router.delete("/delete/:id", auth.authenticateToken, (req, res, next) => {
  const id = req.params.id;
  let query = "DELETE FROM bill WHERE id=?";
  connection.query(query, [id], (err, results) => {
    if (!err) {
      if (results.affectedRows == 0) {
        return res.status(404).json({ message: "bill id is not exist" });
      }
      return res.status(200).json({ message: "delete bill successfully" });
    } else {
      return Helper.dbErrorReturn(err, res);
    }
  });
});

module.exports = router;
