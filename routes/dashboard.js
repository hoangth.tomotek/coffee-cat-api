const express = require("express");
const connection = require("../connection.js");
const router = express.Router();
let auth = require("../services/authentication.js");
let checkRole = require("../services/checkRole.js");
const Helper = require("../helper.js").Helper;

router.get(
  "/details",
  auth.authenticateToken,
  checkRole.checkRole,
  (req, res, next) => {
    let categoryCount;
    let productCount;
    let billCount;
    let query = "SELECT COUNT(id) AS categoryCount FROM category";
    connection.query(query, (err, results) => {
      if (!err) {
        categoryCount = results[0].categoryCount;
      } else {
        return Helper.dbErrorReturn(err, res);
      }
    });

    query = "SELECT COUNT(id) AS productCount FROM product";
    connection.query(query, (err, results) => {
      if (!err) {
        productCount = results[0].productCount;
      } else {
        return Helper.dbErrorReturn(err, res);
      }
    });

    query = "SELECT COUNT(id) AS billCount FROM bill";
    connection.query(query, (err, results) => {
      if (!err) {
        billCount = results[0].billCount;
        let data = {
          category: categoryCount,
          product: productCount,
          bill: billCount,
        };
        return res.status(200).json(data);
      } else {
        return Helper.dbErrorReturn(err, res);
      }
    });
  }
);

module.exports = router;
