require("dotenv").config();
const express = require("express");
const connection = require("../connection.js");
const router = express.Router();
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
let auth = require("../services/authentication.js");
let checkRole = require("../services/checkRole.js");
let Helper = require("../helper.js").Helper;

router.post("/signup", (req, res) => {
  let user = req.body;
  let query = "SELECT email, password, role, status FROM user WHERE email=?";
  connection.query(query, [user.email], (err, results) => {
    if (!err) {
      if (results.length <= 0) {
        query =
          "INSERT INTO user (name,contactNumber,email,password,status,role)" +
          "VALUES (?,?,?,?,1,'user')";
        let hashPassword = Helper.hashPassword(user.password);
        connection.query(
          query,
          [
            user.name.toUpperCase(),
            user.contactNumber,
            user.email,
            hashPassword,
          ],
          (err, results) => {
            if (!err) {
              const newUserId = results.insertId;
              query = "INSERT INTO cart (userId) VALUES (?)";
              connection.query(query, [newUserId], (err, result) => {
                if (!err) {
                  return res.status(200).json({
                    message: "successfully registered",
                  });
                } else {
                  return Helper.dbErrorReturn(err, res);
                }
              });
            } else {
              return Helper.dbErrorReturn(err, res);
            }
          }
        );
      } else {
        return res.status(400).json({ message: "email already exist" });
      }
    } else {
      return Helper.dbErrorReturn(err, res);
    }
  });
});

router.post("/signin", (req, res) => {
  let user = req.body;
  let query = "SELECT email, password, role, status FROM user WHERE email=?";
  connection.query(query, [user.email], (err, results) => {
    if (!err) {
      if (
        results.length <= 0 ||
        !Helper.comparePassword(results[0].password, user.password)
      ) {
        return res.status(401).json({
          message: "incorrect email or password",
        });
      } else if (results[0].status == 0) {
        return res.status(401).json({
          message: "wait for admin aprroval",
        });
      } else if (Helper.comparePassword(results[0].password, user.password)) {
        const response = { email: results[0].email, role: results[0].role };
        const accessToken = jwt.sign(response, process.env.ACCESS_TOKEN, {
          expiresIn: "12h",
        });

        res.status(200).json({ token: accessToken });
      } else {
        return res.status(400).json({
          message: "something went wrong. please try again later",
        });
      }
    } else {
      return Helper.dbErrorReturn(err, res);
    }
  });
});

let transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: process.env.EMAIL,
    pass: process.env.PASSWORD,
  },
});

router.post("/forgotpassword", (req, res) => {
  const user = req.body;
  let query = "SELECT email, password FROM user WHERE email=?";
  connection.query(query, [user.email], (err, results) => {
    if (!err) {
      if (results.length > 0) {
        return res.status(200).json({
          message: "password sents successfully to your email",
        });
      } else {
        let mailOptions = {
          from: process.env.EMAIL,
          to: results[0].email,
          subject: "password by coffee cat management system",
          html:
            "<p><b>your login details for Coffee Cat management system</b><br><b>email: </b>" +
            results[0].email +
            "<br><b>password: </b>" +
            results[0].password +
            '<br><a href="http://localhost:4200/">Click here to login</a></p>',
        };
        transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log("email sent: " + info.response);
          }
        });
        return res.status(200).json({
          message: "password sent successfully to your email",
        });
      }
    } else {
      return Helper.dbErrorReturn(err, res);
    }
  });
});

router.get(
  "/get",
  auth.authenticateToken,
  checkRole.checkRole,
  (req, res, next) => {
    let query =
      "SELECT id,name,contactNumber,email,status FROM user WHERE role='user'";
    connection.query(query, (err, results) => {
      if (!err) {
        return res.status(200).json(results);
      } else {
        return Helper.dbErrorReturn(err, res);
      }
    });
  }
);

router.patch(
  "/update",
  auth.authenticateToken,
  checkRole.checkRole,
  (req, res) => {
    let user = req.body;
    let query = "UPDATE user SET status=? WHERE id=?";
    connection.query(query, [user.status, user.id], (err, results) => {
      if (!err) {
        if (results.affectedRows == 0) {
          return res.status(404).json({ message: "user id is not exist" });
        }
        return res.status(200).json({ message: "update user successfully" });
      } else {
        return Helper.dbErrorReturn(err, res);
      }
    });
  }
);

router.get("/checkToken", auth.authenticateToken, (req, res, next) => {
  return res.status(200).json({ message: true });
});

router.get("/getInfo", auth.authenticateToken, (req, res, next) => {
  const email = res.locals.email;
  let query = "SELECT * FROM user WHERE email = ?";
  connection.query(query, [email], (err, results) => {
    if (!err) {
      if (results.length == 0) {
        return res.status(404).json({
          error: "user not found",
        });
      }
      return res.status(200).json(results[0]);
    } else {
      return Helper.dbErrorReturn(err, res);
    }
  });
});

router.post("/changePassword", auth.authenticateToken, (req, res, next) => {
  const user = req.body;
  const email = res.locals.email;
  let query = "SELECT * FROM user WHERE email=? AND status = 1";
  connection.query(query, [email], (err, results) => {
    if (!err) {
      if (!Helper.comparePassword(results[0].password, user.oldPassword)) {
        return res.status(400).json({ message: "incorrect old password" });
      } else if (
        Helper.comparePassword(results[0].password, user.oldPassword)
      ) {
        let hashPassword = Helper.hashPassword(user.newPassword);
        let query = "UPDATE user SET password=? WHERE email=?";
        connection.query(query, [hashPassword, email], (err, results) => {
          if (!err) {
            return res
              .status(200)
              .json({ message: "password updated successfully" });
          } else {
            return Helper.dbErrorReturn(err, res);
          }
        });
      } else {
        return res
          .status(400)
          .json({ message: "something went wrong. please try again later" });
      }
    } else {
      return Helper.dbErrorReturn(err, res);
    }
  });
});

router.patch("/updateInfo", auth.authenticateToken, (req, res, next) => {
  const name = req.body.name;
  const contactNumber = req.body.contactNumber;
  const email = res.locals.email;
  let query = "SELECT * FROM user WHERE email = ?";
  connection.query(query, [email], (err, results) => {
    if (!err) {
      if (results.length == 0) {
        return res.status(404).json({ error: "user not found" });
      }
      query = "UPDATE user SET status = 1";
      if (name) {
        query += " , name = ?";
      }
      if (contactNumber) {
        query += " , contactNumber = ?";
      }
      query += " WHERE email = ?";
      connection.query(query, [name, contactNumber, email], (err, results) => {
        if (!err) {
          return res.status(200).json({
            message: "user info updated successfully",
          });
        } else {
          return Helper.dbErrorReturn(err, res);
        }
      });
    } else {
      return Helper.dbErrorReturn(err, res);
    }
  });
});

module.exports = router;
