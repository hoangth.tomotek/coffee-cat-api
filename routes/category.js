require("dotenv").config();
const express = require("express");
const connection = require("../connection.js");
const router = express.Router();
let auth = require("../services/authentication.js");
let checkRole = require("../services/checkRole.js");
const Helper = require("../helper.js").Helper;

router.post(
  "/add",
  auth.authenticateToken,
  checkRole.checkRole,
  (req, res, next) => {
    const category = req.body;
    let query = "INSERT INTO category (name) VALUES (?)";
    connection.query(query, [category.name], (err, results) => {
      if (!err) {
        return res.status(200).json({ message: "category added successfully" });
      } else {
        return Helper.dbErrorReturn(err, res);
      }
    });
  }
);

router.get("/get", (req, res, next) => {
  let query = "SELECT * FROM category ORDER BY name";
  connection.query(query, (err, results) => {
    if (!err) {
      return res.status(200).json(results);
    } else {
      return Helper.dbErrorReturn(err, res);
    }
  });
});

router.patch(
  "/update",
  auth.authenticateToken,
  checkRole.checkRole,
  (req, res, next) => {
    const category = req.body;
    let query = "UPDATE category SET name = ? WHERE id=?";
    connection.query(query, [category.name, category.id], (err, results) => {
      if (!err) {
        if (results.affectedRows == 0) {
          return res.status(404).json({ message: "category id is not exist" });
        }
        return res
          .status(200)
          .json({ message: "update category successfully" });
      } else {
        return Helper.dbErrorReturn(err, res);
      }
    });
  }
);

router.delete(
  "/delete/:id",
  auth.authenticateToken,
  checkRole.checkRole,
  (req, res, next) => {
    const id = req.params.id;
    let query = "DELETE FROM category WHERE id=?";
    connection.query(query, [id], (err, results) => {
      if (!err) {
        if (results.affectedRows == 0) {
          return res.status(404).json({ message: "category id is not exist" });
        }
        return res
          .status(200)
          .json({ message: "delete category successfully" });
      } else {
        return Helper.dbErrorReturn(err, res);
      }
    });
  }
);

module.exports = router;
