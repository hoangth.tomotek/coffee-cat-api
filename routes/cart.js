require("dotenv").config();
const express = require("express");
const connection = require("../connection.js");
const router = express.Router();
let auth = require("../services/authentication.js");
let checkRole = require("../services/checkRole.js");
const Helper = require("../helper.js").Helper;
const UPLOADS_PATH = `http://localhost:${process.env.PORT}/uploads/`;

router.get("/getByUser", auth.authenticateToken, (req, res, next) => {
  const email = res.locals.email;
  let query = "SELECT * FROM user WHERE email = ?";
  connection.query(query, [email], (err, userResult) => {
    if (!err) {
      if (userResult.length == 0) {
        return res.status(404).json({
          error: "User not found",
        });
      }
      const userId = userResult[0].id;
      query = "SELECT * FROM cart WHERE userId = ?";
      connection.query(query, [userId], (err, cartResult) => {
        if (!err) {
          if (cartResult.length == 0) {
            return res.status(404).json({
              error: "Cart not found",
            });
          }
          const cartId = cartResult[0].id;
          //   const data = {
          //     quantity: cartResult[0].quantity,
          //     totalAmount: cartResult[0].totalAmount,
          //   };
          query = `
                SELECT ci.productId, p.name, p.price, p.image, ci.quantity, ci.total
                FROM cart_items ci LEFT JOIN product p ON ci.productId = p.id
                WHERE ci.cartId = ? AND ci.isDeleted = 0
                ORDER BY ci.id DESC
            `;
          connection.query(query, [cartId], (err, cartItems) => {
            if (!err) {
              // data = cartItems.map((item) => ({
              //   productId: item.id,
              //   name: item.name,
              //   price: item.price,
              //   quantity: item.quantity,
              //   total: item.total,
              // }));

              cartItems = cartItems.map((product) => {
                product.image = UPLOADS_PATH + product.image;
                return product;
              });
              return res.status(200).json(cartItems);
            } else {
              return Helper.dbErrorReturn(err, res);
            }
          });
        } else {
          return Helper.dbErrorReturn(err, res);
        }
      });
    } else {
      return Helper.dbErrorReturn(err, res);
    }
  });
});

router.post("/addProductToCart", auth.authenticateToken, (req, res, next) => {
  const email = res.locals.email;
  const productId = req.body.id;

  let query = "SELECT * FROM user WHERE email = ?";
  connection.query(query, [email], (err, userResult) => {
    if (!err) {
      if (userResult.length == 0) {
        return res.status(404).json({
          error: "User not found",
        });
      }
      const userId = userResult[0].id;
      query = "SELECT * FROM cart WHERE userId = ?";
      connection.query(query, [userId], (err, cartResult) => {
        if (!err) {
          if (cartResult.length == 0) {
            return res.status(404).json({
              error: "Cart not found",
            });
          }
          const cartId = cartResult[0].id;
          query = "SELECT price FROM product WHERE id = ? AND status = 1";
          connection.query(query, [productId], (err, product) => {
            if (!err) {
              const price = product[0].price;
              query =
                "SELECT * FROM cart_items WHERE cartId = ? AND productId = ? AND isDeleted = 0";
              connection.query(query, [cartId, productId], (err, cartItems) => {
                if (!err) {
                  if (cartItems.length > 0) {
                    query =
                      "UPDATE cart_items SET quantity = ?, total = ? WHERE cartId = ? AND productId = ? AND isDeleted = 0";
                    let quantity = cartItems[0].quantity + 1;
                    let total = cartItems[0].total + price;
                    connection.query(
                      query,
                      [quantity, total, cartId, productId],
                      (err, result) => {
                        if (!err) {
                          return res.status(200).json({
                            message: "add product to cart successfully",
                          });
                        } else {
                          return Helper.dbErrorReturn(err, res);
                        }
                      }
                    );
                  } else if (cartItems.length == 0) {
                    query =
                      "INSERT INTO cart_items (cartId, productId, total) VALUES (?, ?, ?)";
                    connection.query(
                      query,
                      [cartId, productId, price],
                      (err, result) => {
                        if (!err) {
                          return res.status(200).json({
                            message: "add product to cart successfully",
                          });
                        } else {
                          return Helper.dbErrorReturn(err, res);
                        }
                      }
                    );
                  }
                } else {
                  return Helper.dbErrorReturn(err, res);
                }
              });
            } else {
              return Helper.dbErrorReturn(err, res);
            }
          });
        } else {
          return Helper.dbErrorReturn(err, res);
        }
      });
    } else {
      return Helper.dbErrorReturn(err, res);
    }
  });
});

router.post("/updateQuantity", auth.authenticateToken, (req, res, next) => {
  const email = res.locals.email;
  const productId = req.body.productId;
  const quantity = req.body.quantity;

  let query = "SELECT * FROM user WHERE email = ?";
  connection.query(query, [email], (err, userResult) => {
    if (!err) {
      if (userResult.length == 0) {
        return res.status(404).json({
          error: "User not found",
        });
      }
      const userId = userResult[0].id;
      query = "SELECT * FROM cart WHERE userId = ?";
      connection.query(query, [userId], (err, cartResult) => {
        if (!err) {
          if (cartResult.length == 0) {
            return res.status(404).json({
              error: "Cart not found",
            });
          }
          const cartId = cartResult[0].id;
          query = `
                SELECT p.id, p.name, p.price, ci.quantity, ci.total
                FROM cart_items ci LEFT JOIN product p ON ci.productId = p.id
                WHERE ci.cartId = ? AND ci.productId = ? AND ci.isDeleted = 0
            `;
          connection.query(query, [cartId, productId], (err, cartItems) => {
            if (!err) {
              const quantityProduct = cartItems[0].quantity;
              if (quantityProduct == 1 && quantity == -1) {
                query =
                  "UPDATE cart_items SET isDeleted = 1 WHERE cartId = ? AND productId = ?";
                connection.query(query, [cartId, productId], (err, result) => {
                  if (!err) {
                    return res.status(200).json({
                      message: "update quantity successfully",
                    });
                  } else {
                    return Helper.dbErrorReturn(err, res);
                  }
                });
              } else if (quantityProduct > 1) {
                query =
                  "UPDATE cart_items SET quantity = ?, total = ? WHERE cartId = ? AND productId = ?";
                let updateQuantity;
                const price = cartItems[0].price;
                let total = cartItems[0].total;
                if (quantity == 1) {
                  updateQuantity = quantityProduct + 1;
                  total += price;
                } else if (quantity == -1) {
                  updateQuantity = quantityProduct - 1;
                  total -= price;
                }
                connection.query(
                  query,
                  [updateQuantity, total, cartId, productId],
                  (err, result) => {
                    if (!err) {
                      return res.status(200).json({
                        message: "update quantity successfully",
                      });
                    } else {
                      return Helper.dbErrorReturn(err, res);
                    }
                  }
                );
              }
            } else {
              return Helper.dbErrorReturn(err, res);
            }
          });
        } else {
          return Helper.dbErrorReturn(err, res);
        }
      });
    } else {
      return Helper.dbErrorReturn(err, res);
    }
  });
});

router.post("/removeProduct", auth.authenticateToken, (req, res, next) => {
  const email = res.locals.email;
  const productId = req.body.productId;

  let query = "SELECT * FROM user WHERE email = ?";
  connection.query(query, [email], (err, userResult) => {
    if (!err) {
      if (userResult.length == 0) {
        return res.status(404).json({
          error: "User not found",
        });
      }
      const userId = userResult[0].id;
      query = "SELECT * FROM cart WHERE userId = ?";
      connection.query(query, [userId], (err, cartResult) => {
        if (!err) {
          if (cartResult.length == 0) {
            return res.status(404).json({
              error: "Cart not found",
            });
          }
          const cartId = cartResult[0].id;
          query = "UPDATE cart_items SET isDeleted = 1 WHERE cartId = ? ";

          if (!Helper.checkNullOrEmpty(productId)) {
            query += " AND productId = ?";
          }
          connection.query(query, [cartId, productId], (err, cartItems) => {
            if (!err) {
              return res.status(200).json({
                message: "remove product from cart successfully",
              });
            } else {
              return Helper.dbErrorReturn(err, res);
            }
          });
        } else {
          return Helper.dbErrorReturn(err, res);
        }
      });
    } else {
      return Helper.dbErrorReturn(err, res);
    }
  });
});

module.exports = router;
